Patch ssh_ipykernel to undo appending '/bin/python' to the remote python path.

- python : `our.ssh_ipykernel`
- gitlab : `patch-ssh_ipykernel`

---
ndiamistrator @ 2022-03-24 09:49:22
