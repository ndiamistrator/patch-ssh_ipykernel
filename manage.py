from ssh_ipykernel.manage import add_kernel
import sys
import argparse

from .kernel import patch


def parser(parser=None):
    if parser is None:
        parser = argparse.ArgumentParser(add_help=False)

    parser = argparse.ArgumentParser(add_help=False)
    optional = parser.add_argument_group("optional arguments")
    optional.add_argument(
        "--help",
        "-h",
        action="help",
        default=argparse.SUPPRESS,
        help="show this help message and exit",
    )
    optional.add_argument(
        "--display-name", "-d", type=str, help="kernel display name (default is host name)"
    )
    optional.add_argument(
        "--sudo",
        "-s",
        action="store_true",
        help="sudo required to start kernel on the remote machine",
    )
    optional.add_argument(
        "--timeout", "-t", type=int, help="timeout for remote commands", default=5
    )
    optional.add_argument(
        "--env",
        "-e",
        nargs="*",
        help="environment variables for the remote kernel in the form: VAR1=value1 VAR2=value2",
    )

    required = parser.add_argument_group("required arguments")
    required.add_argument("--host", "-H", required=True, help="remote host")
    required.add_argument("--python", "-p", required=True, help="remote python_path")

    return parser


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    _parser = parser()
    args = _parser.parse_args(argv)

    if args.env:
        env = " ".join(args.env)

    patch()
    add_kernel(
        host=args.host,
        display_name=args.host if args.display_name is None else args.display_name,
        local_python_path=sys.executable,
        remote_python_path=args.python,
        sudo=args.sudo,
        env=env,
        timeout=args.timeout,
        module=__package__,
    )

if __name__ == "__main__":
    sys.exit(main())
