import ssh_ipykernel.kernel

__all__ = 'SshKernel', 'patch'


class SshKernel(ssh_ipykernel.kernel.SshKernel):
    def __init__(S, *args, **kwargs):
        S.__python_full_path_patchit = True
        super().__init__(*args, **kwargs)
        import sys
        print(f'ssh_ipykernel: instance patched: {S.python_full_path}', file=sys.stderr)

    @property
    def python_full_path(S):
        return S.__python_full_path

    @python_full_path.setter
    def python_full_path(S, value):
        if S.__python_full_path_patchit:
            S.__python_full_path = value.parent.parent
            S.__python_full_path_patchit = False
        else:
            S.__python_full_path = value

    @python_full_path.deleter
    def python_full_path(S):
        del S.__python_full_path


_patched = False

def patch():
    import sys
    if issubclass(ssh_ipykernel.kernel.SshKernel, SshKernel):
        print('ssh_ipykernel: class already patched', file=sys.stderr)
    else:
        ssh_ipykernel.SshKernel = SshKernel
        print('ssh_ipykernel: class patched', file=sys.stderr)
